/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokedex;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Moacir
 */
public class Treinador {
    private String nome;
	private String senha;
	//private List <Pokemon> pokemon;
	
	
	public Treinador() {
		
	}

	
	
	public String getNome() {
		return nome;
	}



	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}


	public void setSenha(String senha) {
		this.senha = senha;
	}

 
   
   public boolean entrar(String nome ,String senha) {
	   		
	    try {
	      FileReader arq = new FileReader("Usuario.txt");
	      BufferedReader lerArq = new BufferedReader(arq);
	 
	      String nomeAr = lerArq.readLine();
	      String senhaAr = lerArq.readLine();
	      if(nomeAr == nome && senhaAr == senha) {
	    	  arq.close();
	    	  return true;	
		   }
		   else {
			   arq.close();
				
			   return false;
		   }
	          
	    } catch (IOException e) {
	        System.err.printf("Erro na abertura do arquivo: %s.\n",
	          e.getMessage());
	        return false;
	    }
		
	
	  }
    public boolean testarCadastro(String nome,String Senha)throws IOException{
         
	   	    try {
	      File arq = new File("Usuario.txt");
	      if(arq.exists()){
	    	System.out.println("Já existe um usuario!");
                    return (false);
	    	  }
	      else {
                  Treinador treinador = new Treinador();
                   treinador.setNome(nome);
                   treinador.setSenha(senha);
	    	  FileWriter arqv = new FileWriter("usuario.txt");
	    	    PrintWriter gravarArqv = new PrintWriter(arqv);
	    	    gravarArqv.printf(nome+"\r\n");
	    	    gravarArqv.printf(senha+"\r\n");
	    	   arqv.close();
	    	    
	    	    System.out.printf("Cadastro realizado com sucesso");
	    	
                    return (true);
	      
	   	    }
	    } catch (IOException e) {
	        System.err.printf("Erro na abertura do arquivo: %s.\n",
	          e.getMessage());
	    }
        return false;
	 
     }
	   
   
}
